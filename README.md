# Trabajo Práctico Nº1 - Organización del Computador II

## Fórmula Resolvente en ASSEMBLER

El trabajo consiste en crear un programa en la arquitectura de IA-32, que dado un _a_ , _b_ y un _c_ calcula la [fórmula resolvente](https://es.wikipedia.org/wiki/Deducci%C3%B3n_de_la_f%C3%B3rmula_de_Bhaskara):

<img src="https://latex.codecogs.com/svg.latex?\Large&space;x=\frac{-b\pm\sqrt{b^2-4ac}}{2a}" title="\Large x=\frac{-b\pm\sqrt{b^2-4ac}}{2a}" />

Crearemos un programa en C que solicite los valores de _a_ , _b_ y _c_ por consola. Estos valores pueden ser de punto flotante.

Finalmente compilaremos y linkearemos por separado la parte de ASM y de C, y luego obtendremos un sólo ejecutable que solicite los valores y muestre el resultado por consola.

## Requisitos de Ejecución

Se debe tener en una misma carpeta los siguientes archivos:

* [main.c](Código/main.c)
* [resolvente.asm](Código/resolvente.asm)
* [exe.sh](Código/exe.sh)

## Pasos para la Ejecución

* Ingresar a la carpeta donde están los tres archivos

* Una vez allí ejecutamos el [exe.sh](Código/exe.sh)

<p align="center"><img src="Capturas/1.png"/></p>

* La primera vez ejecutará el programa entero, y nos creará un archivo *run*, el cual utilizaremos para ejecuciones posteriores.

<p align="center"><img src="Capturas/2.png"/></p>

<p align="center"><img src="Capturas/3.png"/></p>

* Ahora si queremos ejecutar más veces, ejecutamos el *run*

<p align="center"><img src="Capturas/4.png"/></p>

<p align="center"><img src="Capturas/5.png"/></p>


## Ejercicios de Gestión de Memoria
* [Ejercicios GM](Ejercicios/Ejercicios.md)


## Ejercicio 4 FPU

* [Ejercicio FPU](Ejercicios/ejercicio_4_FPU.md)

## Autor ✒️

* **Kevin Ruiz**
