extern printf

SECTION .data
    numbera dq 0.0
    numberb dq 0.0
    numberc dq 0.0
    menos_b dq 0.0

    bcuadrado dq 0.0
    cuatroAC dq 0.0

    cuatro dq 4.0
    dos dq 2.0
    menos_uno dq -1.0

    radicando dq 0.0
    divisor dq 0.0
    dividendo dq 0.0
    raiz_uno dq 0.0
    raiz_dos dq 0.0

    format_f db "%f",10,0
    texto_una_raiz db "Hay una raíz y es: ",0
    texto_dos_raices db "Hay dos raíces y son: ",10,0


SECTION .text
global SOLVER

SOLVER:
    ENTER:
        push ebp
        mov ebp, esp
  
    GUARDAR_DATOS: 
        fld dword[ebp + 8]
        fstp qword[numbera]

        fld dword[ebp + 12]
        fstp qword[numberb]

        fld dword[ebp + 16]
        fstp qword[numberc]

    CALCULAR_DIVISOR:
        fld qword[numbera]
        fld qword[dos]
        fmul
        fstp qword[divisor]

    MENOS_B:
        fld qword[numberb]
        fld qword[menos_uno]
        fmul
        fstp qword[menos_b]

    CALCULAR_BCUADRADO:
        fld qword[numberb]
        fmul st0, st0
        fstp qword[bcuadrado]
    
    CALCULAR_CUATROAC:
        fld qword[numbera]
        fld qword[cuatro]
        fmul
        fld qword[numberc]
        fmul
        fstp qword[cuatroAC]

    CALCULAR_RADICANDO:
        fld qword[bcuadrado]
        fld qword[cuatroAC]
        fsub
        fstp qword[radicando]

    APLICAR_RAIZ:
        fld qword[radicando]
        fsqrt
        fstp qword[radicando]

    ;Verifo después de aplicar la raiz porque asumo que el radicando es mayor o igual a cero
    VERIFICAR_CANT_RAICES:
        mov eax, [radicando + 4]
        cmp eax, 0
        je UNA_RAIZ
        ja DOS_RAICES

    UNA_RAIZ:
        fld qword[menos_b]
        fstp qword[dividendo]

        fld qword[dividendo]
        fld qword[divisor]
        fdiv
        fstp qword[raiz_uno]

        push texto_una_raiz
        call printf

        push dword[raiz_uno + 4]
        push dword[raiz_uno]
        push format_f
        call printf

        add esp, 16
        jmp LEAVE

    DOS_RAICES:
        RAMA_SUMA:
            fld qword[menos_b]
            fld qword[radicando]
            fadd
            fstp qword[dividendo]

            fld qword[dividendo]
            fld qword[divisor]
            fdiv
            fstp qword[raiz_uno]

        RAMA_RESTA:
            fld qword[menos_b]
            fld qword[radicando]
            fsub
            fstp qword[dividendo]

            fld qword[dividendo]
            fld qword[divisor]
            fdiv
            fstp qword[raiz_dos]

        push texto_dos_raices
        call printf

        push dword[raiz_uno + 4]
        push dword[raiz_uno]
        push format_f
        call printf

        push dword[raiz_dos + 4]
        push dword[raiz_dos]
        push format_f
        call printf   

        add esp, 28 

    LEAVE:
        mov esp, ebp
        pop ebp
