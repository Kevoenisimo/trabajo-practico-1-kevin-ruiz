# Ejercicios 4,6,7 Gestión de Memoria y ejercicio 4 FPU

## Gestión de memoria

4)
	a) Con paginación de un solo nivel, el número máximo de entradas de la tabla de páginas coincide
	con el número máximo de páginas, es decir:

	Calculamos cuanta memoria virtual puedo direccionar
	y la dividimos por el tamaño de las páginas:

		2^32 -> 4Gb. Puedo direccionar 4Gb de memoria virtual.

	El tamaño de la página es igual al del frame (4mb):
		
		4Gb/4Mb -> 2^32/2^22 -> 2^10 -> 1024.
		
	Con paginación de un sólo nivel tengo 1024 entradas.


	b) Con tabla de paginación invertido tendría una entrada por cada página de memoria FÍSICA.
	Divido la memoria física (3GB) por el tamaño de los frames (4Mb)
		
		3GB/4Mb -> 2^30 * 3 / 2^22 -> 3*2^8 -> 3*256 -> 768.

	c) Mi formato de direcciones sería:
	   	
		<xx,yy,zz> = 32 bits
 
	   Calculo el offset (zz) -> Tengo 4Mb de páginas. Para manejarlos necesito 22 bits de offset.
	   
		<xx,yy,22> 
	
	   Ahora 32-22 -> 10 bits de página.
	  
	   Mi propuesta de formato de direcciones es:

<p align="center"><img src="Ejercicio_4_C_GM.png"/></p>

	   				


6) 

	DS -> 1 < 4000 -> Dir FISICA = 5000 + 1 = 5001

	CS -> 520 < 25000 -> Dir FISICA = 10000 + 520 = 10520

	SS -> 350 < 3500 -> Dir FISICA = 50 + 350 = 400

	SS -> 4000 < 3500 -> TRAP

7) 

<h3>Primera fila de páginas</h3>

<p align="center"><img src="Primera_Fila/1.png"/></p>

<p align="center"><img src="Primera_Fila/2.png"/></p>

<p align="center"><img src="Primera_Fila/3.png"/></p>

<p align="center"><img src="Primera_Fila/4.png"/></p>

<p align="center"><img src="Primera_Fila/5.png"/></p>


<h3>Segunda fila de páginas</h3>

<p align="center"><img src="Segunda_Fila/1.png"/></p>

<p align="center"><img src="Segunda_Fila/2.png"/></p>

<p align="center"><img src="Segunda_Fila/3.png"/></p>

<p align="center"><img src="Segunda_Fila/4.png"/></p>

<p align="center"><img src="Segunda_Fila/5.png"/></p>







