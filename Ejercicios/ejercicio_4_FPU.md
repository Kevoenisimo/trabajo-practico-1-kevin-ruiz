# Ejercicio 4 FPU

## Código en ASM

	extern printf

	SECTION .data
    	vector dd 15.0, 12.0, 1.0,7.0,22.0,3.5,11.75,51.25,8.5
    	suma dq 0.0
    	format db "La suma del vector es: %f",10,0

	SECTION .text
		global ejercicio_cuatro

	ejercicio_cuatro:
    	mov eax, vector
    	fld dword[eax]
    	mov ebx,1

   	mov ecx,8
    CICLO:
        fld dword[eax + 4*ebx]
        fadd
        fst qword[suma]
        inc ebx
    loop CICLO

    RETURN:
        push ebp
        mov ebp, esp

        push dword[suma + 4]
        push dword[suma]
        push format
        call printf

        add esp, 12
        mov esp, ebp
        pop ebp
        
        mov eax, 1
        mov ebx, 0
        int 0x80	

## Código de C

	#include <stdio.h>

	extern void ejercicio_cuatro();
	int main()
	{
    	ejercicio_cuatro();
    	return 0;
	}

## BASH

	nasm -f elf32 ejercicio_4_FPU.asm -o ejercicio.o
	gcc -m32 ejercicio_cuatro.c ejercicio.o -o run
	./run

